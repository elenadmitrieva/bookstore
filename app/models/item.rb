class Item < ApplicationRecord
  belongs_to :book
  belongs_to :cart

  include ActiveModel::Validations
  validates_with Tools::ItemValidator
end
