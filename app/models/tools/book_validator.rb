module Tools
  class BookValidator < ActiveModel::Validator
    def validate(book)
      %w(name description price).each do |attribute|
        next if book.send(attribute).present?

        book.errors.add :base, "#{attribute.humanize} can't be blank"
      end
    end
  end
end
