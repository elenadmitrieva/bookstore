module Tools
  class ItemValidator < ActiveModel::Validator
    def validate(item)
      %w(quantity price).each do |attribute|
        next if item.send(attribute).to_i > 0

        item.errors.add :base, "#{attribute.humanize} must be greater than 0"
      end
    end
  end
end
