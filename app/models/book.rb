class Book < ApplicationRecord
  has_many :items

  include ActiveModel::Validations
  validates_with Tools::BookValidator
end
