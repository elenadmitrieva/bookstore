class ItemDecorator < SimpleDelegator
  def total_price
    price * quantity
  end
end
