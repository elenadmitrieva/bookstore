class CartDecorator < SimpleDelegator
  def total_price
    items.to_a.sum { |item| ItemDecorator.new(item).total_price }
  end
end
