class CartManager
  def self.cart
    Cart.last || Cart.new
  end

  def self.add_item(book)
    cart_items = cart.items
    current_item = cart_items.find_by(book_id: book.id)
    if current_item
      current_item.quantity += 1
    else
      current_item = cart_items.build(book_id: book.id)
      current_item.price = book.price
    end
    current_item.save
  end

  def self.remove_item(id)
    item = cart.items.find(id)
    item.destroy
  end
end
