class CartsController < ApplicationController

  def show
    @cart = Cart.find(params[:id])
    @decorator_cart = CartDecorator.new(@cart)
  end
end
