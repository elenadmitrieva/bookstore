class BookstoreController < ApplicationController
  def index
    @books = Book.order(:name).limit(10)
  end
end
