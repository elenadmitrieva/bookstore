class ItemsController < ApplicationController

  def create
    book = Book.find(params[:book_id])
    CartManager.add_item(book)
    redirect_to root_url
  end

  def update
    @cart = CartManager.cart
    @item = @cart.items.find(params[:id])
    @item.update_attributes(quantity: params[:quantity])
  end

  def destroy
    CartManager.remove_item(params[:id])
    respond_to do |format|
      format.html { redirect_to cart_url(CartManager.cart) }
      format.js { redirect_to cart_url(CartManager.cart) }
    end
  end
end
