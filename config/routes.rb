Rails.application.routes.draw do
  resources :carts, only: [:show]
  resources :items, only: [:create, :update, :destroy]
  root 'bookstore#index'
end
