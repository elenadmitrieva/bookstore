require 'rails_helper'

describe BookstoreController do
  let!(:book) { create :book }

  it "responds successfully" do
    get :index
    expect(response).to be_success
    expect(response.status).to be(200)
    expect(response).to render_template :index
  end
end