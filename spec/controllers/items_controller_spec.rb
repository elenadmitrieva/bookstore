require 'rails_helper'

describe ItemsController do
  let!(:item) { create(:item, quantity: 1) }
  let(:item_with_book) { build(:item, quantity: 2, book: book) }
  let(:book) { create :book }


  describe "POST create" do
    it "creates a new item" do
      expect { post :create, params: {book_id: book} }.to change(Item, :count).by(1)
    end

    it "add existing item" do
      item_with_book.save
      expect { post :create, params: {book_id: book} }.to_not change(Item, :count)
    end

    it "responds successfully with redirect to root_url" do
      post :create, params: {book_id: book}
      expect(response).to redirect_to root_url
    end
  end

  describe "DELETE destroy" do
    it "destroy single item" do
      expect { delete :destroy, params: {id: item.id} }.to change(Item, :count).by(-1)
      expect(response).to redirect_to cart_url
    end

    it "destroy item with quantity > 1" do
      item_with_book.save
      expect { delete :destroy, params: {id: item_with_book.id} }.to change(Item, :count).by(-1)
      expect(response).to redirect_to cart_url
    end
  end
end