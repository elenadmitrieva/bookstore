require 'rails_helper'

describe CartsController do
  let(:cart) { create :cart }

  it "responds successfully and render template" do
    get :show, params: { id: cart.id }
    expect(response).to be_success
    expect(response.status).to be(200)
    expect(response).to render_template :show
  end
end