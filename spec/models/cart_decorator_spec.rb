require 'rails_helper'

describe CartDecorator do
  let(:cart) { create :cart }
  let!(:item_1) { create(:item, quantity: 3, price: 10.00, cart: cart) }
  let!(:item_2) { create(:item, quantity: 2, price: 5.00, cart: cart) }

  it 'returns the total price' do
    expect(described_class.new(cart).total_price).to eq(40.00)
  end
end