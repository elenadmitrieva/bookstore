require 'rails_helper'

describe Cart do
  it 'has many items' do
    should have_many(:items)
  end
end