require 'rails_helper'

describe Item do

  it 'belongs to cart' do
    should belong_to(:cart)
  end

  it 'belongs to book' do
    should belong_to(:book)
  end

  describe 'validations' do
    let(:item) { create(:item, quantity: 10, price: 5.99) }

    it 'should be valid when all required fields are positive numbers' do
      expect(item).to be_valid
    end

    it 'should be invalid when price is negative number' do
      item.price = -122
      expect(item).to_not be_valid
    end

    it 'should be invalid when quantity is blank' do
      item.quantity = nil
      expect(item).to_not be_valid
    end

    it 'should be invalid when quantity is negative number' do
      item.quantity = -1
      expect(item).to_not be_valid
    end
  end
end