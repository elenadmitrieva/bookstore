require 'rails_helper'

describe Book do
  it 'has many items' do
    should have_many(:items)
  end

  describe 'validations' do
    let(:book) { create :book }

    it 'should be valid when all required fields present' do
      expect(book).to be_valid
    end

    it 'should be invalid when name is blank' do
      book.name = nil
      expect(book).to_not be_valid
    end

    it 'should be invalid when description is blank' do
      book.description = nil
      expect(book).to_not be_valid
    end

    it 'should be invalid when price is blank' do
      book.price = nil
      expect(book).to_not be_valid
    end
  end
end