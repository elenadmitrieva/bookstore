require 'rails_helper'

describe ItemDecorator do
  let(:item) { create(:item, quantity: 2, price: 10.00) }

  it 'returns the total price' do
    expect(described_class.new(item).total_price).to eq(20.00)
  end
end