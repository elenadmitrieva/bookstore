FactoryGirl.define do
  factory :cart do
  end

  factory :book do
    name { Faker::HarryPotter.book }
    description { Faker::HarryPotter.quote }
    price { Faker::Number.decimal(2) }
  end

  factory :item do
    quantity {Faker::Number.between(1, 30) }
    price { Faker::Number.decimal(2) }
    association :cart
    association :book
  end
end