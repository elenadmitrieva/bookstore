[
    ["1984", "Written in 1948, 1984 was George Orwell’s chilling prophecy about the future. And while 1984 has come and gone, his dystopian vision of a government that will do anything to control the narrative is timelier than ever...", "9,99"],
    ["A Brief History of Time ", "A landmark volume in science writing by one of the great minds of our time, Stephen Hawking’s book explores such profound questions as: How did the universe begin—and what made its start possible? ", "18,00"],
    ["Gone Girl", "Gillian Flynn On a warm summer morning in North Carthage, Missouri, it is Nick and Amy Dunne’s fifth wedding anniversary...", "15,00"],
    ["A Wrinkle in Time", "Madeleine L'Engle Madeleine L'Engle's ground-breaking science fiction and fantasy classic, soon to be a major motion picture.", "6,99"],
    ["Bel Canto", "Ann Pratchett’s award winning, New York Times bestselling Bel Canto balances themes of love and crisis as disparate characters learn that music is their only common language.", "17,99"],
    ["Charlotte's Web", "This beloved book by E. B. White, author of Stuart Little and The Trumpet of the Swan, is a classic of children's literature that is 'just about perfect.' This paper-over-board edition includes a foreword by two-time Newbery winning author Kate DiCamillo.", "8,99"],
    ["Fahrenheit 451", "Ray Bradbury’s internationally acclaimed novel Fahrenheit 451 is a masterwork of twentieth-century literature set in a bleak, dystopian future.", "15,99"],
    ["The Lord of the Rings", "The 50th anniversary one-volume edition of J.R.R. Tolkien's epic. 'An extraordinary work - pure excitement.' - New York Times Book Review.", "20,00"],
    ["Harry Potter and the Sorcerer's Stone", "Harry Potter has no idea how famous he is. That's because he's being raised by his miserable aunt and uncle who are terrified Harry will learn that he's really a wizard, just as his parents were. But everything changes when Harry is summoned to attend an infamous school for wizards, and he begins to discover some clues about his illustrious birthright. ", "10,99"],
    ["Me Talk Pretty One Day", "A recent transplant to Paris, humorist David Sedaris, bestselling author of 'Naked', presents a collection of his strongest work yet, including the name story about his hilarious attempt to learn French. A number one national bestseller now in paperback.", "16,00"],
    ["Lolita", "When it was published in 1955, Lolita immediately became a cause célèbre because of the freedom and sophistication with which it handled the unusual erotic predilections of its protagonist. But Vladimir Nabokov's wise, ironic, elegant masterpiece owes its stature as one of the twentieth century's novels of record not to the controversy its material aroused but to its author's use of that material to tell a love story almost shocking in its beauty and tenderness.", "24,00"]
].each do |(name, text, price)|
  Book.create! name: name, description: text, price: price
end
