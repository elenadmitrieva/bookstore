# DEPLOYMENT DOC

This README would normally document whatever steps are necessary to get the
application up and running.

* Clone the App
```
#!code
git clone https://elenadmitrieva@bitbucket.org/elenadmitrieva/bookstore.git
```
* Install Ruby version 2.4.0

```
#!code
brew update
brew upgrade rbenv ruby-build
rbenv install 2.4.0
```
* Install Rails version 5.0.1

```
#!code
gem install rails -v 5.0.1
rbenv rehash
```
* System dependencies
```
#!code
bundle install
```
* Database
```
#!code
rake db:create db:migrate db:seed
```
* How to run the test suite
```
#!code
 bundle exec rspec ./spec
```